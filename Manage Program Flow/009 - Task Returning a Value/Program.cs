﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _9___Task_Returning_a_Value
{
    class Program
    {
        static void Main(string[] args)
        {
            var task = Task.Run(() => 42);

            Console.WriteLine("Task result: {0}", task.Result);  // Calling Result implicitally calls Wait()
            Console.WriteLine("Finished.");
            Console.ReadKey(true);
        }
    }
}
