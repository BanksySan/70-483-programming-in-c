﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _016___Parallel.For_and_parallel.Foreach
{
    class Program
    {
        static void Main(string[] args)
        {
            var random = new Random();

            Parallel.For(0, 10, x => 
                {
                    Thread.Sleep(random.Next(0, 1000));
                    Console.WriteLine("Parallel.For({0}).", x);
                });

            var numbers = Enumerable.Range(0, 10);

            Parallel.ForEach(numbers, x => 
                {
                    Thread.Sleep(random.Next(0, 1000));
                    Console.WriteLine("Parallel.ForEach({0}).", x);
                });

            Console.WriteLine("Finished.");
            Console.ReadKey(true);
        }
    }
}
