﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _018___acync_and_await
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Foo called");
            var result = Foo(5);

            while (result.Status != TaskStatus.RanToCompletion)
            {
                Console.WriteLine("Thread ID: {0}, Status: {1}", Thread.CurrentThread.ManagedThreadId, result.Status);
                Task.Delay(100).Wait();
            }

            Console.WriteLine("Result: {0}", result.Result);
            Console.WriteLine("Finished.");
            Console.ReadKey(true);
        }

        private static async Task<string> Foo(int seconds)
        {
            return await Task.Run(() =>
                {
                    for (int i = 0; i < seconds; i++)
                    {
                        Console.WriteLine("Thread ID: {0}, second {1}.", Thread.CurrentThread.ManagedThreadId, i);
                        Thread.Sleep(TimeSpan.FromSeconds(1));
                    }

                    return "Foo Completed.";
                });
        }
    }
}
