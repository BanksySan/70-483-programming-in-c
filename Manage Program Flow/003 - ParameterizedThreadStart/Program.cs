﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _3___ParameterizedThreadStart
{
    class Program
    {
        static void ThreadMethod(object o)
        {
            for (int i = 0; i < (int)5; i++)
            {
                Console.WriteLine("ThreadProc: {0}", i);
                Thread.Sleep(0);
            }
        }

        static void Main(string[] args)
        {
            Thread t = new Thread(new ParameterizedThreadStart(ThreadMethod));
            t.Start(5);
            t.Join();
            Console.WriteLine("Finished.");
            Console.Read();
        }
    }
}
