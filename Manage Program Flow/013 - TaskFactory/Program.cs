﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _013___TaskFactory
{
    class Program
    {
        static void Main(string[] args)
        {
            var parent = Task.Run(() =>
                {
                    var results = new bool[3];

                    var taskFactory = new TaskFactory(TaskCreationOptions.AttachedToParent, TaskContinuationOptions.ExecuteSynchronously);

                    taskFactory.StartNew(() => results[0] = true);
                    taskFactory.StartNew(() => results[1] = true);
                    taskFactory.StartNew(() => results[2] = true);


                    return results;
                });

            var finalTask = parent.ContinueWith(x => string.Join(", ", x.Result));
            Console.WriteLine("finalTask = {0}", finalTask.Result);
            Console.WriteLine("Finshed.");
            Console.ReadKey(true);
        }
    }
}
