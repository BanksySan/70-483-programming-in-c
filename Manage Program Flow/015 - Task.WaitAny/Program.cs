﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _015___Task.WaitAny
{
    class Program
    {
        static void Main(string[] args)
        {
            var i = 0;

            var tasks = new[]
            {
                Task.Run(() => 
                    {
                        Thread.Sleep(TimeSpan.FromSeconds(1));
                        Console.WriteLine("Task {0}", i++);
                        return i;
                    }),
                    Task.Run(() => 
                    {
                        Thread.Sleep(TimeSpan.FromSeconds(1));
                        Console.WriteLine("Task {0}", i++);
                        return i;
                    }),
                    Task.Run(() => 
                    {
                        Thread.Sleep(TimeSpan.FromSeconds(1));
                        Console.WriteLine("Task {0}", i++);
                        return i;
                    })
            };

            Task.WaitAny(tasks);
            Console.WriteLine("Finished.");
            Console.ReadKey(true);
        }
    }
}
