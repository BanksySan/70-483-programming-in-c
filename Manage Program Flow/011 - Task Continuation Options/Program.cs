﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _011___Task_Continuation_Options
{
    class Program
    {
        static void Main(string[] args)
        {
            var task = Task.Run(() => 42);

            task.ContinueWith(parent => Console.WriteLine("Falted."), TaskContinuationOptions.OnlyOnFaulted);
            task.ContinueWith(parent => Console.WriteLine("Cancelled."), TaskContinuationOptions.OnlyOnCanceled);
            task.ContinueWith(parent => Console.WriteLine("Ran to completion."), TaskContinuationOptions.OnlyOnRanToCompletion);
            Console.WriteLine("Finished.");
            Console.ReadKey(true);
        }
    }
}
