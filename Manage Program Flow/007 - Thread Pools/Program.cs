﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _7___Thread_Pools
{
    class Program
    {
        static void Main(string[] args)
        {
            ThreadPool.QueueUserWorkItem(x => Console.WriteLine("Working on it.  x = {0}", x.GetType().Name));
            Console.WriteLine("Finished.");
            Console.ReadKey(true);
        }
    }
}
