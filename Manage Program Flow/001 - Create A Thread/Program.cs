﻿namespace _1___Create_A_Thread
{
    using System;
    using System.Threading;

    class Program
    {
        static void Main(string[] args)
        {
            var t = new Thread(new ThreadStart(ThreadMethod));

            t.Start();

            for (int i = 0; i < 4; i++)
            {
                Console.WriteLine("Main thread: Do some work.");
                Thread.Sleep(0);
            }

            t.Join();

            Console.WriteLine("Finished.");
            Console.Read();
        }

        static void ThreadMethod()
        {
            for (var i = 0; i < 10; i++)
            {
                Console.WriteLine("threadProc: {0}", i);
                Thread.Sleep(0);
            }
        }
    }
}
