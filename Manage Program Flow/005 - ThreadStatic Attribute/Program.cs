﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _5___ThreadStatic_Attribute
{
    class Program
    {
        /// <summary>
        /// The <see cref="System.ThreadStatic">[ThreadStatic]</see> class forces a fresh variable to be created for each thread.
        /// 
        /// With ThreadStatic, we have two variables which count from 0 to 9 twice, without it, the threads count it from 0 to 19.
        /// </summary>
        [ThreadStatic] public static int _field;

        static void Main(string[] args)
        {
            new Thread(() =>
                {
                    for (int i = 0; i < 10; i++)
                    {
                        Console.WriteLine("Thread A: {0}", _field++);
                    }
                }).Start();

            new Thread(() =>
                {
                    for (int i = 0; i < 10; i++)
                    {
                        Console.WriteLine("Thread B: {0}", _field++);
                    }
                }).Start();

            Console.WriteLine("Threads started");
            Console.ReadKey(true);
        }
    }
}
