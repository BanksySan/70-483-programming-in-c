﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _012___Adding_Child_Tasks
{
    class Program
    {
        static void Main(string[] args)
        {
            var parent = Task.Run(() =>
                {
                    var results = new bool[3];
                    new Task(() => results[0] = true, TaskCreationOptions.AttachedToParent).Start();
                    new Task(() => results[1] = true, TaskCreationOptions.AttachedToParent).Start();
                    new Task(() => results[2] = true, TaskCreationOptions.AttachedToParent).Start();

                    return results;
                });
            
            var finalTask = parent.ContinueWith(x => Console.WriteLine(string.Join(", ", parent.Result)), TaskContinuationOptions.OnlyOnRanToCompletion);

            Console.WriteLine("Finished.");
            Console.ReadKey(true);
        }
    }
}
