﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _8___New_Task
{
    class Program
    {
        static void Main(string[] args)
        {
            var task = Task.Run(() => Console.WriteLine("Task running."));

            task.Wait();
            Console.WriteLine("Finished.");
            Console.ReadKey(true);
        }
    }
}
