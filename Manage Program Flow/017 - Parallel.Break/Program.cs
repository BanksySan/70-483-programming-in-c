﻿using System;
using System.Threading.Tasks;

namespace _017___Parallel.Break
{
    class Program
    {
        static void Main(string[] args)
        {
            ParallelLoopResult result = Parallel.For(0, 10, (int i, ParallelLoopState loopState) =>
                {
                    Console.WriteLine("Iteration: {0}", i);
                    if (i == 5)
                    {
                        Console.WriteLine("Breaking loop @ {0}", i);
                        loopState.Break();
                    }
                });

            Console.WriteLine("Finished.");
            Console.ReadKey(true);
        }
    }
}
