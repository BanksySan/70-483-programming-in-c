﻿namespace BackgroundTreads
{
    using System;
    using System.Threading;

    /// <summary>
    /// If the IsBackground is true, the application will exit straight away.
    /// If Set to false, then the program will print out the message.
    /// </summary>
    class Program
    {
        public static void ThreadMethod()
        {
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("ThreadProc: {0}", i);
                Thread.Sleep(1000);
            }
        }

        static void Main(string[] args)
        {
            Thread t = new Thread(new ThreadStart(ThreadMethod));
            t.IsBackground = true;
            t.Start();
        }
    }
}
