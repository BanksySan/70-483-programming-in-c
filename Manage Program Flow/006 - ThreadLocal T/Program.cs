﻿using System;
using System.Threading;

namespace _6___ThreadLocal_T
{
    class Program
    {
        public static ThreadLocal<int> _field = new ThreadLocal<int>(() => Thread.CurrentThread.ManagedThreadId);
        
        static void Main(string[] args)
        {
            new Thread(() => Console.WriteLine("A: ManagedThreadId: {0}", _field)).Start();
            new Thread(() => Console.WriteLine("B: ManagedThreadId: {0}", _field)).Start();
            new Thread(() => Console.WriteLine("C: ManagedThreadId: {0}", _field)).Start();

            Console.WriteLine("Finished.");
            Console.ReadKey(true);
        }
    }
}
